/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeproject;

/**
 *
 * @author Administrator
 */
public class Rectangle {
    private double h, w;
    
    public Rectangle (double h, double w) {
        this.h = h;
        this.w = w;
    }
    
    public double calArea() {
        return h * w;
    }
    
    public double getH() {
        return h;
    }
    public double getW() {
        return w;
    }
    
    public void setHW(double h, double w) {
        if (h<=0 & w>0) {
            System.out.println("Error: Height(h) must more than zero!!");
            return;
        }
        if (w<=0 & h>0) {
            System.out.println("Error: Width(w) must more than zero!!");
            return;
        }
        if (h<=0 & w<=0) {
            System.out.println("Error: Height(h) and Width(w) must more than zero!!");
            return;
        }
        this.h = h;
        this.w = w;
    }
}
