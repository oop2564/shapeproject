/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeproject;

/**
 *
 * @author Administrator
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(2, 5);
        System.out.println("Area of Rectangle (h = " + rect1.getH() + ", w = " + rect1.getW() + ") is " + rect1.calArea());
        rect1.setHW(3, 7);
        System.out.println("Area of Rectangle (h = " + rect1.getH() + ", w = " + rect1.getW() + ") is " + rect1.calArea());
        rect1.setHW(0, 44);
        System.out.println("Area of Rectangle (h = " + rect1.getH() + ", w = " + rect1.getW() + ") is " + rect1.calArea());
        rect1.setHW(8, 0);
        System.out.println("Area of Rectangle (h = " + rect1.getH() + ", w = " + rect1.getW() + ") is " + rect1.calArea());
        rect1.setHW(0, 0);
        System.out.println("Area of Rectangle (h = " + rect1.getH() + ", w = " + rect1.getW() + ") is " + rect1.calArea());
    }
}
