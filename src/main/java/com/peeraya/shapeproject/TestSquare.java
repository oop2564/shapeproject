/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeproject;

/**
 *
 * @author Administrator
 */
public class TestSquare {
    public static void main(String[] args) {
        Square sq1 = new Square(5);
        System.out.println("Area of Square (s = " + sq1.getS() + ") is " + sq1.calArea());
        sq1.setS(7);
        System.out.println("Area of Square (s = " + sq1.getS() + ") is " + sq1.calArea());
        sq1.setS(0);
        System.out.println("Area of Square (s = " + sq1.getS() + ") is " + sq1.calArea());
    }
}
