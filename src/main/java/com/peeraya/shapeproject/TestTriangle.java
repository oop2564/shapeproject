/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeproject;

/**
 *
 * @author Administrator
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle tri1 = new Triangle(5, 13);
        System.out.println("Area of Triangle (b = " + tri1.getB() + ", h = " + tri1.getH() + ") is " + tri1.calArea());
        tri1.setBH(8, 2);
        System.out.println("Area of Triangle (b = " + tri1.getB() + ", h = " + tri1.getH() + ") is " + tri1.calArea());
        tri1.setBH(0, 7);
        System.out.println("Area of Triangle (b = " + tri1.getB() + ", h = " + tri1.getH() + ") is " + tri1.calArea());
        tri1.setBH(25, 0);
        System.out.println("Area of Triangle (b = " + tri1.getB() + ", h = " + tri1.getH() + ") is " + tri1.calArea());
        tri1.setBH(0, 0);
        System.out.println("Area of Triangle (b = " + tri1.getB() + ", h = " + tri1.getH() + ") is " + tri1.calArea());
    }
}
